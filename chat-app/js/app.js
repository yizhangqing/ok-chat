window.app = {
	serverUrl: "http://192.168.253.7:8080",
	/**
	 * 判断字符串是否为空
	 * @param {Object} str
	 * @example:
	 * undefined -> false
	 * null -> false
	 * "" -> false
	 * " " -> true 
	 */
	isNotEmpty: function(str) {
		if (str && str.length > 0) {
			return true;
		}
		return false;
	},
	/**
	 * 判断字符串是否为空
	 * @param {Object} str
	 * @example:
	 * undefined -> false
	 * null -> false
	 * "" -> false
	 * " " -> false 
	 */
	isNotBlank: function(str) {
		if (str && str.trim() != "") {
			return true;
		}
		return false;
	},
	toast: function(msg, type) {
		var imgType = type == "loading" ? ".gif" : ".png";
		plus.nativeUI.toast(msg, {
			icon: "images/toast/" + type + imgType,
			verticalAlign: "center"
		})
	},
	/**
	 * 将用户登陆信息保存到本地数据存储区
	 * @param {Object} str
	 */
	setUserInfo: function(str) {
		var userInfoStr = JSON.stringify(str);
		plus.storage.setItem("userInfo", userInfoStr);
	},
	/**
	 * 获取当前登陆用户信息
	 */
	getUserInfo: function() {
		var userInfoStr = plus.storage.getItem("userInfo");
		return JSON.parse(userInfoStr);
	},
	/**
	 * 用户退出后删除用户信息
	 */
	delUserInfo: function() {
		plus.storage.removeItem("userInfo");
	}
}
